﻿using System;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SerialPortListener.Serial;
using System.IO;
using System.Threading;
using System.Xml.Linq;

namespace SerialPortListener
{
    public partial class MainForm : Form
    {
        SerialPortManager _spManager;

        //Stream Parsing
        byte [] packet_array;
        int packet_array_idx; //packet_array index 
        enum pack_states { FIND_0X7E,FIND_ESCAPE_CHAR };
        pack_states pack_state;

        enum process_stage
        {
            DOWNLOAD_BINARY=0,
            PARSE_BINARY,
            PARSING,
            SORT_TEXT
        };

        

        private process_stage Stage;
        private String [] FileNames;
        private String FileTimeStamp;
        private String DownloadPath;// = @"c:\ArtikBio_Download";
        private FileStream [] FilesHandle;
        private FileStream[] SeqFilesHandle;
        private UInt32 DownloadedKB;
        private System.Windows.Forms.Timer UiTimer;
        static private byte[] HostStartCommand = { 0x7E, 0x20, 0x10, 0x00, 0x62, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1D, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0xB0, 0x00, 0x7E };
        static private byte[] HostStopCommand =  { 0x7E, 0x20, 0x10, 0x00, 0x62, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1D, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xAF, 0x00, 0x7E };
        private UInt32 [] DatalossCount = { 0, 0 };
        private FileStream RawFileHandle;
        private String RawFileName;//="RawData";
        private BinaryWriter RawDataWriter;
        private BinaryReader RawDataReader;
        delegate void StringArgReturningVoidDelegate(string text);
        delegate void IntArgReturningVoidDelegate(int val);
        Thread ParseWorkerThread;
        private int MaxStreamNumber;

        public MainForm()
        {
            InitializeComponent();

            UserInitialization();
        }

      
        private void UserInitialization()
        {
            int i;
            _spManager = new SerialPortManager();
            SerialSettings mySerialSettings = _spManager.CurrentSerialSettings;
            serialSettingsBindingSource.DataSource = mySerialSettings;
            portNameComboBox.DataSource = mySerialSettings.PortNameCollection;
            baudRateComboBox.DataSource = mySerialSettings.BaudRateCollection;
            dataBitsComboBox.DataSource = mySerialSettings.DataBitsCollection;
            parityComboBox.DataSource = Enum.GetValues(typeof(System.IO.Ports.Parity));
            stopBitsComboBox.DataSource = Enum.GetValues(typeof(System.IO.Ports.StopBits));

            _spManager.NewSerialDataRecieved += new EventHandler<SerialDataEventArgs>(_spManager_NewSerialDataRecieved);
            this.FormClosing += new FormClosingEventHandler(MainForm_FormClosing);

            //get data from app properties
            MaxStreamNumber = Properties.Settings.Default.MaxStream;
            RawFileName = Properties.Settings.Default.BinFileName;
            DownloadPath = @Properties.Settings.Default.DownloadPath;

            pack_state = pack_states.FIND_0X7E;  // start looking for 0x7e
            packet_array = new byte[Properties.Settings.Default.MaxPacketSize]; // allocate 400 bytes


            FileNames = new String[MaxStreamNumber]; // one file name for each string
            FilesHandle = new FileStream[MaxStreamNumber];
            SeqFilesHandle = new FileStream[MaxStreamNumber];
            ParseWorkerThread = null;

            for (i = 0; i < MaxStreamNumber; i++)
            {
                FileNames[i] = i.ToString() + "_";
                FilesHandle[i] = null;
                SeqFilesHandle[i] = null;
            }

            RawFileHandle = null;
            UiTimer = new System.Windows.Forms.Timer();
            UiTimer.Interval=2000; //msec
            UiTimer.Tick += new EventHandler(TimedEvent);
            UiTimer.Stop();
            Stage = process_stage.DOWNLOAD_BINARY;
            progressBarParse.Value = 0;

            // create directory
            if (!System.IO.Directory.Exists(DownloadPath))
            {
                System.IO.Directory.CreateDirectory(DownloadPath);
                log_status("Directory:" + DownloadPath + " Was Created!\r\n");
            }
        }


        private void TimedEvent(object sender, EventArgs e)
        {
            
            double Val = (Convert.ToDouble(DownloadedKB) / 1000.0);
            String KBSofar = Val.ToString();

            if (KBSofar != llDownloded.Text)
                llDownloded.Text = KBSofar;
            else
            {
                if (KBSofar != "0")
                {
                    RawDataWriter.Flush();
                    RawDataWriter.Close();
                    UiTimer.Stop();
                    Stage = process_stage.PARSE_BINARY;
                    tbData.Clear();
                    tbData.Text = "";
                    log_status("Binary Download done ....\r\n");
                    log_status("Click Parse Binary Button to continue ....\r\n");

                }
            }

        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _spManager.Dispose();   
        }

        void log_Parse(int val)
        {
            if (progressBarParse.InvokeRequired)
            {
                IntArgReturningVoidDelegate d = new IntArgReturningVoidDelegate(log_Parse);
                this.Invoke(d, new object[] { val });
            }
            else
            {
                progressBarParse.Value=val;
                progressBarParse.Refresh();
                
            }
        }

        void  log_KB(String text)
        {
            if (llDownloded.InvokeRequired)
            {
                StringArgReturningVoidDelegate d = new StringArgReturningVoidDelegate(log_KB);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                llDownloded.Text = text;
            }
        }

        void log_FCS(String text)
        {
            if (llFcs.InvokeRequired)
            {
                StringArgReturningVoidDelegate d = new StringArgReturningVoidDelegate(log_FCS);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                llFcs.Text = text;
            }
        }

        void log_SEQ(String text)
        {
            if (llSeq.InvokeRequired)
            {
                StringArgReturningVoidDelegate d = new StringArgReturningVoidDelegate(log_SEQ);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                llSeq.Text = text;
            }
        }


        void log_data(String DataStr)
        {

            if (tbData.InvokeRequired)
            {
                StringArgReturningVoidDelegate d = new StringArgReturningVoidDelegate(log_data);
                this.Invoke(d, new object[] { DataStr });
            }
            else
            {
                if (tbData.TextLength > 10000)
                {
                    tbData.Text = tbData.Text.Remove(0, tbData.TextLength - 10000);
                }

                tbData.AppendText(DataStr);
                tbData.ScrollToCaret();
            }

            
        }

        void log_status(String StatusStr)
        {

            if (tbFilesOpened.InvokeRequired)
            {
                StringArgReturningVoidDelegate d = new StringArgReturningVoidDelegate(log_status);
                this.Invoke(d, new object[] { StatusStr });
            }
            else
            {
                if (tbFilesOpened.TextLength > 10000)
                    tbFilesOpened.Text = tbFilesOpened.Text.Remove(0, tbFilesOpened.TextLength - 10000);

                tbFilesOpened.AppendText(StatusStr);
                tbFilesOpened.ScrollToCaret();
            }

            
        }


        System.IO.FileStream open_stream_files(int stream_type)
        {
            String FullFileName;
            String FullPathFile;
            System.IO.FileStream fs;

            // create directory
            if (!System.IO.Directory.Exists(DownloadPath))
            {
                System.IO.Directory.CreateDirectory(DownloadPath);
                log_status("Directory:" + DownloadPath + " Was Created!\r\n");
            }
            

            // build full data file name + path
            FullFileName = FileNames[stream_type] + FileTimeStamp + ".csv";
            FullPathFile = System.IO.Path.Combine(DownloadPath, FullFileName);
            if (!System.IO.File.Exists(FullPathFile))
            {
                using (fs = System.IO.File.Create(FullPathFile))
                {
                    log_status("File:" + FullPathFile + " Was Created!\r\n");
                    return fs;
                }
            }
            else
            {
                Console.WriteLine("File \"{0}\" already exists.", FullPathFile);
                log_status("File:" + FullPathFile + " Already Exists ...\r\n");
                return null;
            }

        }

        System.IO.FileStream open_seq_files(int stream_type)
        {
            String FullFileName;
            String FullPathFile;
            System.IO.FileStream fs;

            // create directory
            if (!System.IO.Directory.Exists(DownloadPath))
            {
                System.IO.Directory.CreateDirectory(DownloadPath);
                log_status("Directory:" + DownloadPath + " Was Created!\r\n");
            }


            // build full seq file name + path
            FullFileName = "Seq_"+stream_type.ToString()+"_"+ FileTimeStamp + ".csv";
            FullPathFile = System.IO.Path.Combine(DownloadPath, FullFileName);
            if (!System.IO.File.Exists(FullPathFile))
            {
                using (fs = System.IO.File.Create(FullPathFile))
                {
                    log_status("File:" + FullPathFile + " Was Created!\r\n");
                    return fs;
                }
            }
            else
            {
                Console.WriteLine("File \"{0}\" already exists.", FullPathFile);
                log_status("File:" + FullPathFile + " Already Exists ...\r\n");
                return null;
            }

        }


        void parse_stream_data(byte [] stream_packet, int packet_length)
        {
            // Protocol header is 5 bytes, then packet header start by the follwing structure:
            //typedef struct _Stream_Header_t
            //{
            //    uint32_t m_streamtype;    --> offset 5    bytes
            //    uint32_t m_sequencecount; --> offset 9   bytes 
            //    uint32_t m_timestamp;     --> offset 13   bytes
            //    uint32_t m_timestampend;  --> offset 17   bytes
            //    uint32_t m_samplecount;   --> offset 21   bytes
            //}
            //StreamHeader_t;
            
            UInt32 SampleCount,i,SeqNum;
            double TimeStamp;
            UInt64 TimeStampLong;
            float OutPutVal;
            int StreamType;
            UInt16 CalculatedFCS = 0;
            UInt16 RcvdFCS = 0;
            byte PacketType = (byte)(stream_packet[1] >> 4); //get protocol packet type 
            byte[] floatArray = new byte[4];

            //make sure this is a data packet 
            if (PacketType != 1 || packet_length <10)
                return; 

            StreamType = (int)(stream_packet[8] << 24 | stream_packet[7] << 16 | stream_packet[6] << 8 | stream_packet[5]);
            SeqNum = (UInt32)(stream_packet[12] << 24 | stream_packet[11] << 16 | stream_packet[10] << 8 | stream_packet[9]);
            // since revision 0.12.0 timestamp is double
            TimeStamp = BitConverter.ToDouble(stream_packet,13);
            TimeStampLong = (UInt64)(TimeStamp * 1000.0);
            SampleCount = (UInt32)(stream_packet[24] << 24 | stream_packet[23] << 16 | stream_packet[22] << 8 | stream_packet[21]);
            RcvdFCS = (UInt16)(stream_packet[packet_length - 1] << 8 | stream_packet[packet_length - 2]);

            // do not collect for uneeded stream types 
            if (StreamType >= MaxStreamNumber)
            {
                return; // get next packet
            }

            // Calculate frame checksum
            for (i=1;i< packet_length - 2; i++)
            {
                CalculatedFCS += stream_packet[i];
            }

            // if the FCS do not match - exit and drop frame
            if(CalculatedFCS!= RcvdFCS)
            {
                log_status("FCS Error! Packet Type:" + StreamType.ToString() + "\r\n");
                DatalossCount[0]++;
                return;
            }

            if(!cbHideLog.Checked)           
                log_data("Packet Type:" + StreamType.ToString() + " Seq:"+ SeqNum.ToString() + " Timestamp:" + TimeStampLong.ToString() + " Samples:" + SampleCount.ToString()+"\r\n");

            // Open files for this stream
            if (FilesHandle[StreamType] == null)
            {
                FilesHandle[StreamType] = open_stream_files(StreamType);
                SeqFilesHandle[StreamType] = open_seq_files(StreamType);

            }

            var CSV = new StringBuilder();

            // get sample and save to file
            for (i=0;i<(SampleCount*4);i+=4)
            {
                /* convert to float */
                floatArray[0] = stream_packet[25 + i];
                floatArray[1] = stream_packet[26 + i];
                floatArray[2] = stream_packet[27 + i];
                floatArray[3] = stream_packet[28 + i];
                OutPutVal = BitConverter.ToSingle(floatArray, 0);

                var newLine = string.Format("{0},{1},", TimeStampLong.ToString(), OutPutVal.ToString());
                CSV.AppendLine(newLine);
            }

            File.AppendAllText(FilesHandle[StreamType].Name, CSV.ToString());

            // get seq number and save to file
            String Seq_newLine = string.Format("{0},{1}\r\n", SeqNum.ToString(), TimeStampLong.ToString());
            
            File.AppendAllText(SeqFilesHandle[StreamType].Name, Seq_newLine);

        }

        void _spManager_NewSerialDataRecieved(object sender, SerialDataEventArgs e)
        {
            if (this.InvokeRequired)
            {
                // Using this.Invoke causes deadlock when closing serial port, and BeginInvoke is good practice anyway.
                this.BeginInvoke(new EventHandler<SerialDataEventArgs>(_spManager_NewSerialDataRecieved), new object[] { sender, e });
                return;
            }

            // Create the raw data file
            if(RawFileHandle==null)
            {
                String FullFileName = RawFileName + FileTimeStamp + ".bin";
                String FullPathFile = System.IO.Path.Combine(DownloadPath, FullFileName);
                if (!File.Exists(FullPathFile))
                {
                    RawFileHandle = new FileStream(FullPathFile, FileMode.Create);
                    {
                        log_status("File:" + FullPathFile + " Was Created!\r\n");
                        RawDataWriter = new BinaryWriter(RawFileHandle);
                    }
                }
                else
                {
                    Console.WriteLine("File \"{0}\" already exists.", FullPathFile);
                    log_status("File:" + FullPathFile + " Already Exists ...\r\n");
       
                }
            }

            RawDataWriter.Write(e.Data, 0, e.Data.Length);
            RawDataWriter.Flush();
            
            DownloadedKB += (UInt32)e.Data.Length;
            log_data("."); // odot for every packet recieved from uart 
            

        }


        private UInt64 [] sort_seq_files(FileStream fs)
        {
            
            try
            {
                UInt32 i;
                UInt64[] Values=null;

                log_status("Sorting file Seq number:\r\n");

                if (fs != null)
                {
                    log_status(fs.Name + ".....");
                    string[] lines = File.ReadAllLines(fs.Name);
                    Values = new UInt64[lines.Length];
                    
                    for(i=0;i<lines.Length;i++)
                    {
                        Values[i] = Convert.ToUInt64(lines[i].Trim(','));
                    }

                    System.Array.Sort(Values);

                }
                log_status("done! \r\n");
                return Values;
            }
            catch (System.IO.IOException e)
            {
                Console.WriteLine("Error Message = {0}", e.Message);
                MessageBox.Show("Error",e.Message);
                return null;
            }
        }

        private void sort_files(FileStream fs)
        {
            try
            {
                log_status("Sorting file Data:\r\n");

                if (fs != null)
                {
                    log_status(fs.Name + ".....");
                    string[] lines = File.ReadAllLines(fs.Name);
                    var data = lines;
                    var sorted = data.Select(line => new
                    {
                        SortKey = UInt64.Parse(line.Split(',')[0]),
                        Line = line
                    })
                                    .OrderBy(x => x.SortKey)
                                    .Select(x => x.Line);
                    File.WriteAllLines(fs.Name + ".sorted.csv", sorted);
                }
                log_status("done! \r\n");
            }
            catch (System.IO.IOException e)
            {
                Console.WriteLine("Error Message = {0}", e.Message);
                MessageBox.Show("Error",e.Message);
            }
        }


        private void parse_binary_file(String FileToParse)
        {
            String FileName = "";

            if(FileToParse!="")
            {
                FileName = FileToParse;
            }
            else
            {
                FileName = RawFileHandle.Name;
            }

            using  (RawDataReader = new BinaryReader(File.Open(FileName, FileMode.Open)))
            {
                {
                    
                    // Position and length variables.
                    long pos = 0;
                    long percent = 0;
                    long percent_shadow = 0;

                    // Use BaseStream.
                    long length = (long)(RawDataReader.BaseStream.Length);

                    while (pos < length)
                    {
                        percent = Convert.ToInt64((Convert.ToDouble(pos) / Convert.ToDouble(length))* 100.0);
                        if ((percent-percent_shadow)>=1)
                        {
                            percent_shadow = percent;
                            log_Parse((int)percent);
                        }
                        

                        // Read byte
                        byte b = RawDataReader.ReadByte();
                        switch (pack_state)
                        {
                            case pack_states.FIND_0X7E:  // looking for first 0x7e
                                if (b == 0x7e)
                                {
                                    packet_array_idx = 0;
                                    packet_array[packet_array_idx] = 0x7e;
                                    pack_state = pack_states.FIND_ESCAPE_CHAR;
                                    packet_array_idx++;
                                }

                                break;

                            case pack_states.FIND_ESCAPE_CHAR:
                                if (b == 0x7e) // found second 0x7e - close packet
                                {
                                    packet_array[packet_array_idx] = 0x7e;

                                    if (packet_array_idx > 2) // this is a real packet
                                    {
                                        //send to next stage
                                        parse_stream_data(packet_array, packet_array_idx);

                                    }
                                    pack_state = pack_states.FIND_0X7E;
                                    packet_array_idx = 0;
                                }
                                else if (b == 0x7d)  //found stuffing char
                                {
                                    b = RawDataReader.ReadByte(); // read next byte
                                    pos++;   // skip stuff char
                                    packet_array[packet_array_idx] = (byte)(b ^ 0x20); // get real value (XOR 0x20)
                                    packet_array_idx++;
                                }
                                else // regular char
                                {
                                    packet_array[packet_array_idx] =b;
                                    packet_array_idx++;
                                }
                                break;

                            default:
                                pack_state = pack_states.FIND_0X7E;
                                packet_array_idx = 0;
                                break;
                        }
                        // Advance our position variable.
                        pos++;
                    }
                }

                RawDataReader.Close();

            }

        }


        private void check_seq_number_gaps(FileStream fs)
        {
            try
            {

                if (fs != null)
                {
                    String SortedFileName = fs.Name + ".sorted.csv";
                    if (System.IO.File.Exists(SortedFileName))
                    {
                        log_status("Checking for Seq gaps file:" + fs.Name + ".....\r\n");
                        string[] lines = File.ReadAllLines(SortedFileName);
                        UInt32 i;
                        UInt32 Seq = 0;
                        UInt32 ShadowSeq = 0;
                        String[] DataStr;
                        UInt32 Gaps = 0;
                        for (i = 0; i < lines.Length; i++)
                        {
                            DataStr = lines[i].Split(',');
                            Seq = Convert.ToUInt32(DataStr[0]);
                            if ((int)(Seq - ShadowSeq) > 1 && ShadowSeq != 0)
                            {
                                log_status("Seq, Number Error!:" + " Seq:" + Seq.ToString() + " Shadow:" + ShadowSeq.ToString() + "\r\n");
                                DatalossCount[1]++;
                                Gaps++;
                            }
                            ShadowSeq = Seq;
                        }
                        log_status("File " + fs.Name + ": Gaps found: " + Gaps.ToString() + "\r\n");
                    }
                }

            }
            catch (System.IO.IOException e)
            {
                Console.WriteLine("Error Message = {0}", e.Message);
                MessageBox.Show("Error", e.Message);
            }
        }



        // Handles the "Start Listening"-buttom click event
        private void btnStart_Click(object sender, EventArgs e)
        {

            if (Stage == process_stage.DOWNLOAD_BINARY)
            {
                Application.UseWaitCursor = true;
                Cursor.Current = Cursors.WaitCursor;

                log_status("Binary Download Started ...\r\n");
                FileTimeStamp = DateTime.Now.ToString("yyyy-MMMM-dd_HH-mm-ss-tt");
                DownloadedKB = 0;
                RawFileHandle = null;
                log_KB(DownloadedKB.ToString());
                _spManager.StartListening();
                _spManager.SerialDataSend(HostStartCommand);
                UiTimer.Start();
                log_KB("0");
                log_FCS("0");
                log_SEQ("0");

                Application.UseWaitCursor = false;
                Cursor.Current = Cursors.Arrow;
            }
            
        }


        // Handles the "Stop Listening"-buttom click event
        private void btnStop_Click(object sender, EventArgs e)
        {
            int i; 
            _spManager.SerialDataSend(HostStopCommand);
            _spManager.StopListening();

            
            Stage = process_stage.DOWNLOAD_BINARY;
            log_status("Listening stopped ... state machine reset ...\r\n");
            log_status("To Restart  Press the Dowoload Binary Button ...\r\n");

            if (ParseWorkerThread != null)
            {
                if (ParseWorkerThread.IsAlive) // stopping thread in the middle --> cleanup
                {
                    ParseWorkerThread.Abort();
                    ParseWorkerThread = null;

                    for (i = 0; i < MaxStreamNumber; i++)
                    {
                        if (FilesHandle[i] != null)
                        {

                            FilesHandle[i].Close();
                            SeqFilesHandle[i].Close();

                            // Clean interim raw files
                            System.IO.File.Delete(FilesHandle[i].Name);
                            System.IO.File.Delete(SeqFilesHandle[i].Name);
                            SeqFilesHandle[i] = null;
                            FilesHandle[i] = null;
                            RawDataReader.Close();
                            pack_state = pack_states.FIND_0X7E;
                        }
                    }
                    log_status("Parse Binary Stopped... Collected Data might be invalid !!!\r\n");
                    Application.UseWaitCursor = false;
                    Cursor.Current = Cursors.Arrow;
                }
            }
        }

        
        private void Parse_Thread(String FileToParse)
        {
            int i = 0;

            log_status("Binary Parsing Started ...\r\n");
            _spManager.SerialDataSend(HostStopCommand);
            _spManager.StopListening();
            DatalossCount[0] = 0;
            DatalossCount[1] = 0;
            log_FCS("0");
            log_SEQ("0");
            log_Parse(0);
            parse_binary_file(FileToParse);
            log_status("Binary Parsing Ended ...\r\n");
            
            // close all open files
            for (i = 0; i < MaxStreamNumber; i++)
            {
                if (FilesHandle[i] != null)
                {
                    if (cbSortOutFiles.Checked)
                    {
                        log_status("Sorting Started ...\r\n");
                        sort_files(FilesHandle[i]);
                        sort_files(SeqFilesHandle[i]);
                        check_seq_number_gaps(SeqFilesHandle[i]);
                        log_status("Sorting Ended ...\r\n");
                    }

                    FilesHandle[i].Close();
                    SeqFilesHandle[i].Close();

                    // Clean interim raw files
                    if (cbDelIntrimFiles.Checked)
                    {
                        System.IO.File.Delete(FilesHandle[i].Name);
                        System.IO.File.Delete(SeqFilesHandle[i].Name);
                        log_status("Deleted interim CSV files ...\r\n");

                    }
                    SeqFilesHandle[i] = null;
                    FilesHandle[i] = null;

                }

            }

            log_FCS(DatalossCount[0].ToString());
            log_SEQ(DatalossCount[1].ToString());

            Stage = process_stage.DOWNLOAD_BINARY;
            Application.UseWaitCursor = false;
            Cursor.Current = Cursors.Arrow;
            ParseWorkerThread = null;


        }

        private void Parse_Click(object sender, EventArgs e)
        {
            String UserSelectionFile = "";

            if (Stage == process_stage.PARSING)
            {
                MessageBox.Show("Error", "Previous parsing not finished!\r\n To Abort parsing click the \'stop listening\' button ....");
                return;
            }

            //user chose another binary file
            if (textBoxParseFile.Text!="" && textBoxParseFile.Text.EndsWith(".bin") )
            {
                if (System.IO.File.Exists(textBoxParseFile.Text) && (ParseWorkerThread==null)) //file exists and not thread is running
                {
                    Stage = process_stage.PARSE_BINARY;
                    log_status("Parsing User Selection File: " + textBoxParseFile.Text + "\r\n");
                    UserSelectionFile = textBoxParseFile.Text;
                    FileTimeStamp = DateTime.Now.ToString("yyyy-MMMM-dd_HH-mm-ss-tt");
                }
                else
                {
                    if (!System.IO.File.Exists(textBoxParseFile.Text))
                        MessageBox.Show("Error", textBoxParseFile.Text + " does not exist!");
                    else
                        MessageBox.Show("Error","Previous parsing not finished!\r\n To Abort parsing click the \'stop listening\' button ....");

                    return;
                }
            }

            if (Stage == process_stage.PARSE_BINARY)
            {
                Application.UseWaitCursor = true;
                Cursor.Current = Cursors.WaitCursor;
                Stage = process_stage.PARSING;
                ParseWorkerThread = new Thread( () => Parse_Thread(UserSelectionFile));
                ParseWorkerThread.Start();
            }
        }

        private void button1_Click(object sender, EventArgs e)
            {
                tbData.Clear();
            }

            private void button2_Click(object sender, EventArgs e)
            {
                tbFilesOpened.Clear();
            }

            private void MainForm_Load(object sender, EventArgs e)
            {

            }

            private void label6_Click(object sender, EventArgs e)
            {

            }

            private void label1_Click(object sender, EventArgs e)
            {

            }

        private void progressBarParse_Click(object sender, EventArgs e)
        {

        }

        private void fileSystemWatcher1_Changed(object sender, FileSystemEventArgs e)
        {

        }

        private void buttonSelectFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            textBoxParseFile.Text = openFileDialog1.FileName;
            
        }

        
        private void cbDelIntrimFiles_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void buttonSelectDownloadDir_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                textBoxDownloadDir.Text = folderBrowserDialog1.SelectedPath;
                DownloadPath= folderBrowserDialog1.SelectedPath;
            }
          
        }
    } 
}
