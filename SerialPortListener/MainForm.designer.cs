﻿using System;

namespace SerialPortListener
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label baudRateLabel;
            System.Windows.Forms.Label dataBitsLabel;
            System.Windows.Forms.Label parityLabel;
            System.Windows.Forms.Label portNameLabel;
            System.Windows.Forms.Label stopBitsLabel;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            this.baudRateComboBox = new System.Windows.Forms.ComboBox();
            this.serialSettingsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataBitsComboBox = new System.Windows.Forms.ComboBox();
            this.parityComboBox = new System.Windows.Forms.ComboBox();
            this.portNameComboBox = new System.Windows.Forms.ComboBox();
            this.stopBitsComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.tbData = new System.Windows.Forms.TextBox();
            this.btnStop = new System.Windows.Forms.Button();
            this.tbFilesOpened = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.cbSortOutFiles = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.llDownloded = new System.Windows.Forms.Label();
            this.llFcs = new System.Windows.Forms.Label();
            this.llSeq = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnParse = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonSelectDownloadDir = new System.Windows.Forms.Button();
            this.textBoxDownloadDir = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbDelIntrimFiles = new System.Windows.Forms.CheckBox();
            this.buttonSelectFile = new System.Windows.Forms.Button();
            this.textBoxParseFile = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.progressBarParse = new System.Windows.Forms.ProgressBar();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.cbHideLog = new System.Windows.Forms.CheckBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            baudRateLabel = new System.Windows.Forms.Label();
            dataBitsLabel = new System.Windows.Forms.Label();
            parityLabel = new System.Windows.Forms.Label();
            portNameLabel = new System.Windows.Forms.Label();
            stopBitsLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.serialSettingsBindingSource)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // baudRateLabel
            // 
            baudRateLabel.AutoSize = true;
            baudRateLabel.Location = new System.Drawing.Point(10, 59);
            baudRateLabel.Name = "baudRateLabel";
            baudRateLabel.Size = new System.Drawing.Size(61, 13);
            baudRateLabel.TabIndex = 1;
            baudRateLabel.Text = "Baud Rate:";
            // 
            // dataBitsLabel
            // 
            dataBitsLabel.AutoSize = true;
            dataBitsLabel.Location = new System.Drawing.Point(10, 86);
            dataBitsLabel.Name = "dataBitsLabel";
            dataBitsLabel.Size = new System.Drawing.Size(53, 13);
            dataBitsLabel.TabIndex = 3;
            dataBitsLabel.Text = "Data Bits:";
            // 
            // parityLabel
            // 
            parityLabel.AutoSize = true;
            parityLabel.Location = new System.Drawing.Point(10, 113);
            parityLabel.Name = "parityLabel";
            parityLabel.Size = new System.Drawing.Size(36, 13);
            parityLabel.TabIndex = 5;
            parityLabel.Text = "Parity:";
            // 
            // portNameLabel
            // 
            portNameLabel.AutoSize = true;
            portNameLabel.Location = new System.Drawing.Point(10, 32);
            portNameLabel.Name = "portNameLabel";
            portNameLabel.Size = new System.Drawing.Size(60, 13);
            portNameLabel.TabIndex = 7;
            portNameLabel.Text = "Port Name:";
            // 
            // stopBitsLabel
            // 
            stopBitsLabel.AutoSize = true;
            stopBitsLabel.Location = new System.Drawing.Point(10, 140);
            stopBitsLabel.Name = "stopBitsLabel";
            stopBitsLabel.Size = new System.Drawing.Size(52, 13);
            stopBitsLabel.TabIndex = 9;
            stopBitsLabel.Text = "Stop Bits:";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(245, 274);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(82, 13);
            label1.TabIndex = 14;
            label1.Text = "Data Recieved:";
            label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(243, 13);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(72, 13);
            label2.TabIndex = 16;
            label2.Text = "Status/Errors:";
            // 
            // baudRateComboBox
            // 
            this.baudRateComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.serialSettingsBindingSource, "BaudRate", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.baudRateComboBox.FormattingEnabled = true;
            this.baudRateComboBox.Items.AddRange(new object[] {
            "9600",
            "14400",
            "19200",
            "38400",
            "57600",
            "115200"});
            this.baudRateComboBox.Location = new System.Drawing.Point(77, 56);
            this.baudRateComboBox.Name = "baudRateComboBox";
            this.baudRateComboBox.Size = new System.Drawing.Size(121, 21);
            this.baudRateComboBox.TabIndex = 2;
            // 
            // dataBitsComboBox
            // 
            this.dataBitsComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.serialSettingsBindingSource, "DataBits", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.dataBitsComboBox.FormattingEnabled = true;
            this.dataBitsComboBox.Location = new System.Drawing.Point(77, 83);
            this.dataBitsComboBox.Name = "dataBitsComboBox";
            this.dataBitsComboBox.Size = new System.Drawing.Size(121, 21);
            this.dataBitsComboBox.TabIndex = 4;
            // 
            // parityComboBox
            // 
            this.parityComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.serialSettingsBindingSource, "Parity", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.parityComboBox.FormattingEnabled = true;
            this.parityComboBox.Location = new System.Drawing.Point(77, 110);
            this.parityComboBox.Name = "parityComboBox";
            this.parityComboBox.Size = new System.Drawing.Size(121, 21);
            this.parityComboBox.TabIndex = 6;
            // 
            // portNameComboBox
            // 
            this.portNameComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.serialSettingsBindingSource, "PortName", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.portNameComboBox.FormattingEnabled = true;
            this.portNameComboBox.Location = new System.Drawing.Point(77, 29);
            this.portNameComboBox.Name = "portNameComboBox";
            this.portNameComboBox.Size = new System.Drawing.Size(121, 21);
            this.portNameComboBox.TabIndex = 8;
            // 
            // stopBitsComboBox
            // 
            this.stopBitsComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.serialSettingsBindingSource, "StopBits", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.stopBitsComboBox.FormattingEnabled = true;
            this.stopBitsComboBox.Location = new System.Drawing.Point(77, 137);
            this.stopBitsComboBox.Name = "stopBitsComboBox";
            this.stopBitsComboBox.Size = new System.Drawing.Size(121, 21);
            this.stopBitsComboBox.TabIndex = 10;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.baudRateComboBox);
            this.groupBox1.Controls.Add(baudRateLabel);
            this.groupBox1.Controls.Add(this.stopBitsComboBox);
            this.groupBox1.Controls.Add(stopBitsLabel);
            this.groupBox1.Controls.Add(dataBitsLabel);
            this.groupBox1.Controls.Add(this.portNameComboBox);
            this.groupBox1.Controls.Add(this.dataBitsComboBox);
            this.groupBox1.Controls.Add(portNameLabel);
            this.groupBox1.Controls.Add(parityLabel);
            this.groupBox1.Controls.Add(this.parityComboBox);
            this.groupBox1.Location = new System.Drawing.Point(6, 24);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(231, 171);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Serial Port Settings";
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(12, 228);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(97, 23);
            this.btnStart.TabIndex = 12;
            this.btnStart.Text = "Download Binary";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // tbData
            // 
            this.tbData.AccessibleRole = System.Windows.Forms.AccessibleRole.ScrollBar;
            this.tbData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbData.Location = new System.Drawing.Point(246, 300);
            this.tbData.MaxLength = 50000;
            this.tbData.Multiline = true;
            this.tbData.Name = "tbData";
            this.tbData.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbData.Size = new System.Drawing.Size(459, 303);
            this.tbData.TabIndex = 13;
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(6, 271);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(97, 23);
            this.btnStop.TabIndex = 12;
            this.btnStop.Text = "Stop listening";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // tbFilesOpened
            // 
            this.tbFilesOpened.AccessibleRole = System.Windows.Forms.AccessibleRole.ScrollBar;
            this.tbFilesOpened.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFilesOpened.Location = new System.Drawing.Point(246, 37);
            this.tbFilesOpened.MaxLength = 50000;
            this.tbFilesOpened.Multiline = true;
            this.tbFilesOpened.Name = "tbFilesOpened";
            this.tbFilesOpened.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbFilesOpened.Size = new System.Drawing.Size(459, 226);
            this.tbFilesOpened.TabIndex = 15;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(333, 269);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(85, 23);
            this.button1.TabIndex = 17;
            this.button1.Text = "Clear Data";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(331, 8);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(85, 23);
            this.button2.TabIndex = 18;
            this.button2.Text = "Clear Errors";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // cbSortOutFiles
            // 
            this.cbSortOutFiles.AutoSize = true;
            this.cbSortOutFiles.Checked = true;
            this.cbSortOutFiles.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbSortOutFiles.Location = new System.Drawing.Point(112, 190);
            this.cbSortOutFiles.Name = "cbSortOutFiles";
            this.cbSortOutFiles.Size = new System.Drawing.Size(101, 17);
            this.cbSortOutFiles.TabIndex = 19;
            this.cbSortOutFiles.Text = "Sort Output files";
            this.cbSortOutFiles.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(194, 233);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "KByte";
            // 
            // llDownloded
            // 
            this.llDownloded.AutoSize = true;
            this.llDownloded.Location = new System.Drawing.Point(115, 233);
            this.llDownloded.Name = "llDownloded";
            this.llDownloded.Size = new System.Drawing.Size(13, 13);
            this.llDownloded.TabIndex = 23;
            this.llDownloded.Text = "0";
            this.llDownloded.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // llFcs
            // 
            this.llFcs.AutoSize = true;
            this.llFcs.Location = new System.Drawing.Point(18, 419);
            this.llFcs.Name = "llFcs";
            this.llFcs.Size = new System.Drawing.Size(13, 13);
            this.llFcs.TabIndex = 24;
            this.llFcs.Text = "0";
            this.llFcs.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // llSeq
            // 
            this.llSeq.AutoSize = true;
            this.llSeq.Location = new System.Drawing.Point(12, 191);
            this.llSeq.Name = "llSeq";
            this.llSeq.Size = new System.Drawing.Size(13, 13);
            this.llSeq.TabIndex = 25;
            this.llSeq.Text = "0";
            this.llSeq.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(61, 215);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 13);
            this.label6.TabIndex = 26;
            this.label6.Text = "FCS";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(61, 191);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 13);
            this.label7.TabIndex = 27;
            this.label7.Text = "SEQ";
            // 
            // btnParse
            // 
            this.btnParse.Location = new System.Drawing.Point(5, 158);
            this.btnParse.Name = "btnParse";
            this.btnParse.Size = new System.Drawing.Size(98, 23);
            this.btnParse.TabIndex = 28;
            this.btnParse.Text = "Parse Binary";
            this.btnParse.UseVisualStyleBackColor = true;
            this.btnParse.Click += new System.EventHandler(this.Parse_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonSelectDownloadDir);
            this.groupBox2.Controls.Add(this.textBoxDownloadDir);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.cbDelIntrimFiles);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.buttonSelectFile);
            this.groupBox2.Controls.Add(this.btnParse);
            this.groupBox2.Controls.Add(this.textBoxParseFile);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.progressBarParse);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.btnStop);
            this.groupBox2.Controls.Add(this.cbSortOutFiles);
            this.groupBox2.Controls.Add(this.llSeq);
            this.groupBox2.Location = new System.Drawing.Point(6, 204);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(231, 318);
            this.groupBox2.TabIndex = 29;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Control";
            // 
            // buttonSelectDownloadDir
            // 
            this.buttonSelectDownloadDir.Location = new System.Drawing.Point(194, 51);
            this.buttonSelectDownloadDir.Name = "buttonSelectDownloadDir";
            this.buttonSelectDownloadDir.Size = new System.Drawing.Size(31, 21);
            this.buttonSelectDownloadDir.TabIndex = 37;
            this.buttonSelectDownloadDir.Text = "...";
            this.buttonSelectDownloadDir.UseVisualStyleBackColor = true;
            this.buttonSelectDownloadDir.Click += new System.EventHandler(this.buttonSelectDownloadDir_Click);
            // 
            // textBoxDownloadDir
            // 
            this.textBoxDownloadDir.Location = new System.Drawing.Point(9, 78);
            this.textBoxDownloadDir.Name = "textBoxDownloadDir";
            this.textBoxDownloadDir.Size = new System.Drawing.Size(216, 20);
            this.textBoxDownloadDir.TabIndex = 36;
            this.textBoxDownloadDir.Text = "C:\\ArtikBio_Download";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(132, 13);
            this.label4.TabIndex = 35;
            this.label4.Text = "Select download directory:";
            // 
            // cbDelIntrimFiles
            // 
            this.cbDelIntrimFiles.AutoSize = true;
            this.cbDelIntrimFiles.Location = new System.Drawing.Point(112, 214);
            this.cbDelIntrimFiles.Name = "cbDelIntrimFiles";
            this.cbDelIntrimFiles.Size = new System.Drawing.Size(111, 17);
            this.cbDelIntrimFiles.TabIndex = 34;
            this.cbDelIntrimFiles.Text = "Delete interim files";
            this.cbDelIntrimFiles.UseVisualStyleBackColor = true;
            this.cbDelIntrimFiles.CheckedChanged += new System.EventHandler(this.cbDelIntrimFiles_CheckedChanged);
            // 
            // buttonSelectFile
            // 
            this.buttonSelectFile.Location = new System.Drawing.Point(194, 105);
            this.buttonSelectFile.Name = "buttonSelectFile";
            this.buttonSelectFile.Size = new System.Drawing.Size(31, 21);
            this.buttonSelectFile.TabIndex = 33;
            this.buttonSelectFile.Text = "...";
            this.buttonSelectFile.UseVisualStyleBackColor = true;
            this.buttonSelectFile.Click += new System.EventHandler(this.buttonSelectFile_Click);
            // 
            // textBoxParseFile
            // 
            this.textBoxParseFile.Location = new System.Drawing.Point(5, 132);
            this.textBoxParseFile.Name = "textBoxParseFile";
            this.textBoxParseFile.Size = new System.Drawing.Size(216, 20);
            this.textBoxParseFile.TabIndex = 32;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 109);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(178, 13);
            this.label5.TabIndex = 31;
            this.label5.Text = "To select bin file to parse (click \"...\")";
            // 
            // progressBarParse
            // 
            this.progressBarParse.Location = new System.Drawing.Point(7, 249);
            this.progressBarParse.Name = "progressBarParse";
            this.progressBarParse.Size = new System.Drawing.Size(217, 13);
            this.progressBarParse.Step = 1;
            this.progressBarParse.TabIndex = 30;
            this.progressBarParse.Click += new System.EventHandler(this.progressBarParse_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.AddExtension = false;
            this.openFileDialog1.Filter = " bin files (*.bin)|*.bin|All files (*.*)|*.*";
            this.openFileDialog1.InitialDirectory = "c:\\artikbio_download";
            // 
            // cbHideLog
            // 
            this.cbHideLog.AutoSize = true;
            this.cbHideLog.Location = new System.Drawing.Point(424, 273);
            this.cbHideLog.Name = "cbHideLog";
            this.cbHideLog.Size = new System.Drawing.Size(153, 17);
            this.cbHideLog.TabIndex = 35;
            this.cbHideLog.Text = "Hide Log (faster execution)";
            this.cbHideLog.UseVisualStyleBackColor = true;
            this.cbHideLog.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(732, 612);
            this.Controls.Add(this.cbHideLog);
            this.Controls.Add(this.llFcs);
            this.Controls.Add(this.llDownloded);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(label2);
            this.Controls.Add(this.tbFilesOpened);
            this.Controls.Add(label1);
            this.Controls.Add(this.tbData);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "ArtikBio Flash Download tool 1.008";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.serialSettingsBindingSource)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        

        #endregion

        private System.Windows.Forms.BindingSource serialSettingsBindingSource;
        private System.Windows.Forms.ComboBox baudRateComboBox;
        private System.Windows.Forms.ComboBox dataBitsComboBox;
        private System.Windows.Forms.ComboBox parityComboBox;
        private System.Windows.Forms.ComboBox portNameComboBox;
        private System.Windows.Forms.ComboBox stopBitsComboBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.TextBox tbData;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.TextBox tbFilesOpened;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox cbSortOutFiles;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label llDownloded;
        private System.Windows.Forms.Label llFcs;
        private System.Windows.Forms.Label llSeq;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnParse;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ProgressBar progressBarParse;
        private System.Windows.Forms.Button buttonSelectFile;
        private System.Windows.Forms.TextBox textBoxParseFile;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.CheckBox cbDelIntrimFiles;
        private System.Windows.Forms.CheckBox cbHideLog;
        private System.Windows.Forms.Button buttonSelectDownloadDir;
        private System.Windows.Forms.TextBox textBoxDownloadDir;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
    }
}

